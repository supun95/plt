import java.io.BufferedWriter;
import java.io.File;//imports the file from io- io stands for input and output
import java.io.FileNotFoundException;//this signals if the attempt to open the particular file shown by the pathname had failed
import java.io.FileWriter;
import java.io.IOException;//allows to capture all the exceptions in general
import java.io.PrintWriter;//this allows the program to write data in an HTML document
import java.util.ArrayList;
import java.util.Scanner;//used to read the file in this program

public class Requirement1 {

	/*
	 * "throws FileNotFoundException" will handle an error of file not being
	 * found, file being corrupted and other factors that we have no control
	 * over
	 */

	public static void main(String[] args) throws IOException {

		int valid_count = 0;
		int invalid_count = 0;
		int goals = 0;
		int games_played = 0;
		int games_won = 0;
		int games_drawn = 0;
		int games_lost = 0;
		int goals_for = 0;
		int goals_against = 0;
		
		String home_team = null;
		String away_team = null;
		String home_goals = null;
		String  away_goals = null;
		int  home_score = 0;
		int  away_score = 0;
		
		
		
		
		ArrayList<teams> list = new ArrayList<>();

		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the file name: ");
		String filename = input.nextLine();

		// give a choice to the user to pick HTML or plain text
		Scanner scanner = new Scanner(System.in);
		System.out.println("Do you want to generate plain (T)ext or (H)TML");

		boolean generateHTML = false; // this flag is used to identify whether
										// we want HTML or plain text
		String input2 = scanner.nextLine();

		Scanner team = new Scanner(System.in);
		// give a choice to the user to pick a specific team
		System.out
				.println("Select a specific team (<enter> to show all teams): ");
		String teamname = scanner.nextLine();
		
		//ask the user whether they want summary or table output
		System.out.println("Select (S)ummary or (T)able output : ");
		String sum_table = scanner.nextLine();
		
		
		FileWriter fw = new FileWriter(filename + ".html");
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter writer = new PrintWriter(bw);// introduction of PrintWriter
													// which is used to write
													// files into text docs,
													// html docs etc.
		
		
		
	

		if (input2.equalsIgnoreCase("H")) {
			
			generateHTML = true;
				if (teamname.equals("")) { // if the user press enter, it will display this
				// writer.println is added to write into a HTML file
				writer.println("<html><head><link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'><title>Football Scores</title></head><body>");
				// text is enbedded within HTML tags
				writer.println("<h1 style=\"color:#088A85;font-family: 'Montserrat', sans-serif;\">Football Scores</h1>");
				// a table is added to display data
				writer.println("<table border = \"1\" style=\"width:30%;\">");
				writer.println("<tr style=\"background:#088A85;color:white;font-family: 'Montserrat', sans-serif;\"><th>Home Team</th><th>Goals</th><th>Away Teams</th><th>Goals</th></tr>");
			}
		}

		try {

			Scanner results = new Scanner(new File(filename + ".txt"));
			

			/*
			 * hasNext is used to see what's inside the file. hasNext is a
			 * boolean method which continues as long as the file has some
			 * string in it
			 */
			while (results.hasNext()) {
				String s = results.nextLine().trim();// trim is used to take off
														// white spaces

				String[] element = s.split(":");// splitting the line from
												// semicolon

				/*
				 * "try" and "catch" is used inside the if statement to catch
				 * exceptions. in order to validate the football score, it
				 * should be a numerical value. When the string passed and if
				 * the value is an integer, it will be a valid count. However,
				 * if the value is not an integer, it won't be parsed and hence
				 * "catch" is used to capture the value and it will be assigned
				 * as an invalid count
				 */

				if (element.length == 4 && !element[0].trim().isEmpty()
						&& !element[1].trim().isEmpty()
						&& !element[2].trim().isEmpty()
						&& !element[3].trim().isEmpty()) {

						home_team = element[0].trim();
						away_team = element[1].trim();
						home_goals = element[2].trim();
						away_goals = element[3].trim();
												
					try {
						home_score = Integer.parseInt(home_goals);
						away_score = Integer.parseInt(away_goals);
						goals = (home_score + away_score) + goals;
						valid_count++;
											
			
						if (teamname.equalsIgnoreCase(home_team) || teamname.equalsIgnoreCase(away_team)) {
							games_played++;	
							
//							teams gamesplayed = new teams();          
//							gamesplayed.gamesplayed = games_played;
//							list.add(gamesplayed);
							
							
							//check if team name is equal to either home or away team, their goal score
							//is higher than the opposing team
							if ((teamname.equalsIgnoreCase(home_team) && home_score > away_score) || (teamname.equalsIgnoreCase(away_team) && away_score > home_score)) {
								games_won++;							
							}
							
							//check if team name is equal to either home or away team, their goal score
							//is equal the opposing team
							if ((teamname.equalsIgnoreCase(home_team) && (home_score == away_score)) ||(teamname.equalsIgnoreCase(away_team) && (home_score == away_score))) {
								games_drawn++;

							}
							
							//check if team name is equal to either home or away team, their goal score
							//is less than the opposing team
							if ((teamname.equalsIgnoreCase(home_team) && (home_score < away_score)) ||(teamname.equalsIgnoreCase(away_team) && (home_score > away_score))) {
								games_lost++;
							}
							
							//count the number of goals scored
							if (teamname.equalsIgnoreCase(home_team)) {
								goals_for = home_score + goals_for;
								goals_against = away_score + goals_against;
								
							}else {
								goals_for = (away_score) + goals_for;
								goals_against = (home_score) + goals_against;
								}
							
						//if the user picks full table
						}else if (teamname.equals("")) {
							//if the user picks summary
							if (sum_table.equalsIgnoreCase("S")){

							if (generateHTML) { // if and else is used to
												// seperate html from normal
												// text

								writer.println("<tr style=\"font-family: 'Montserrat', sans-serif;\"><td>" + home_team + "</td>" + "<td>" + home_score + "</td>" + "<td>" + away_team + "</td>" + "<td>" + away_score + "</td></tr>");

							} else {
								System.out.println(home_team + " [" + home_score + "] " + "| " + away_team + " [" + away_score + "]");
								
							}

						}
						}
					} catch (NumberFormatException e) {
						invalid_count++;
					}

				}

				else {

					invalid_count++;// this shows if the above conditions are
									// not being met, the result will be invalid
				}
				
				/*storing the team name for requirement 4*/
                teams Team = new teams();		                               
                Team.teamName = home_team;
 //               list.add(Team);
                               
                if(!list.contains(Team)){
                        list.add(Team);
                 }

				// statistics
			}
				
			if (sum_table.equalsIgnoreCase("S")){			
			// if the user press enter
			if (teamname.equals("")) {
				if (generateHTML) {
					writer.println("</table>");
					writer.println(("<br />")); // a blank line is printed
					writer.println("<p style=\"font-family: 'Montserrat', sans-serif;\">Valid match count was " + "<strong>"
							+ valid_count + "</strong>"
							+ ", total goals scored was " + "<strong>" + goals
							+ "</strong>" + "<br />");
					writer.println("Invalid match count was " + "<strong>"
							+ invalid_count + "</strong>" + "</p><br />");
					writer.println("</body></html>");// if generating html then
														// need a suffix
					writer.close();// ends the printwriter
					try {

						// this code triggers the program to open a
						// html file

						Runtime.getRuntime().exec("cmd.exe /C start " + filename + ".html");

					} catch (IOException e) {

						// Could not open the file, show message to use
						System.err.println("Error: Unable to open " + filename);
					}

				} else {
					System.out.println(("\n")); // a blank line is printed
					System.out.println("Valid match count was " + valid_count
							+ ", total goals scored was " + goals);
					System.out.println("Invalid match count was "
							+ invalid_count);

				}

			} else {
				if(generateHTML){
					writer.println(("<br />")); // a blank line is printed
					writer.println("<h2 style=\"font-family: 'Montserrat', sans-serif;color:#088A85;\">"+teamname + "</h2>");
					writer.println("<p>--------------------------------------</p>");
					writer.println("<table style=\"width:15%\">");
					writer.println("<tr style=\"font-family: 'Montserrat', sans-serif;\"><td>Games Played    </td>" + "<td><strong>" + games_played +"</td></tr>");					
					writer.println("<tr style=\"font-family: 'Montserrat', sans-serif;\"><td>Games Won   </td>" + "<td><strong>" + games_won +"</strong></td></tr>");
					writer.println("<tr style=\"font-family: 'Montserrat', sans-serif;\"><td>Games Drawn  </td>" + "<td><strong>" + games_drawn+"</strong></td></tr>");
					writer.println("<tr style=\"font-family: 'Montserrat', sans-serif;\"><td>Games Lost   </td>"+ "<td><strong>"  + games_lost+"</strong></td></tr>");
					writer.println("<tr style=\"font-family: 'Montserrat', sans-serif;\"><td>Goals for  </td>"+ "<td><strong>"  + goals_for+"</strong></td></tr>");
					writer.println("<tr style=\"font-family: 'Montserrat', sans-serif;\"><td>Goals against   </td>"+ "<td><strong>"  + goals_against+"</strong></td></tr>");
					writer.println("</table>");
					writer.println("</body></html>");
					writer.close();
					try {

						// this code triggers the program to open a
						// html file

						Runtime.getRuntime().exec(
								"cmd.exe /C start " + filename + ".html");

					} catch (IOException e) {

						// Could not open the file, show message to use
						System.err.println("Error: Unable to open " + filename);
					}
					
				}else{
					System.out.println(("\n")); // a blank line is printed
					System.out.println(teamname);
					System.out.println("------------------------");
					System.out.println("Games Played: " + games_played);
					System.out.println("Games Won: " + games_won);
					System.out.println("Games Drawn: " + games_drawn);
					System.out.println("Games Lost: " + games_lost);
					System.out.println("Goals for: " + goals_for);
					System.out.println("Goals against: " + goals_against);
				}
			}
			//if the user choose T
			}else if(sum_table.equalsIgnoreCase("T")){				
				if(generateHTML){
					writer.println("<table border = \"1\" style=\"width:30%;\">");
					writer.println("<tr style=\"background:#088A85;color:white;font-family: 'Montserrat', sans-serif;\"><th>Team</th><th>Played</th><th>Goal Difference</th><th>Games Won</th><th>Games Drawn</th><th>Games Lost</th><th>Points</th></tr>");
					for (teams value : list) {
						writer.println("<tr style=\"font-family: 'Montserrat', sans-serif;\"><td>" + value.teamName + "</td>" + "<td>" + value.gamesplayed + "</td>" + "<td>" + value.goaldiference + "</td>" + "<td>" + value.gameswon + "<td>" + value.gamesdrawn +"<td>" + value.gameslost +"<td>" + value.points +"</td></tr>");
						writer.println("</body></html>");
						writer.close();// ends the printwriter
					}
					
					
				}else{
					System.out.println("-------------------------------------------------------------------------------------");
					System.out.println("Team   | Played | Goal Difference | Games Won | Games Drawn | Games Lost | Points ");							
					System.out.println("-------------------------------------------------------------------------------------");					
					for (teams value : list) {
					      System.out.println(value.teamName+ "|" + value.gamesplayed +"|" + value.goaldiference +"|"+ value.gameswon +"|"+ value.gamesdrawn +"|"+ value.gameslost +"|"+ value.points +"|");	      
					    }
					
					
				}
				
				
			}

		} catch (FileNotFoundException e) {

			System.out.println("File does not exist- Rerun the program");
			System.out.println("Files in the folder: ");
			System.out.println("1. results1.txt");
			System.out.println("2. results2.txt");
			System.out.println("3. fbscores.txt");

		}

	}
}
