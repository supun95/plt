public class teams {
	
	    public String teamName;	
		public String home;
		public String away;
		public int goalsfor;
		public int goalsagainst;
		public int gameswon;
		public int gameslost;
		public int gamesdrawn;
		public int gamesplayed;
		public int goaldiference;
		public int points = 0;	
		
		/*prevent repeats*/
		//this code checks if the list contain repeating teams
        @Override
        public boolean equals(Object obj) {
           if (obj == null || obj.getClass() != teams.class) {
               return false;
           }
           teams other = (teams) obj;
           if (!other.teamName.equals(this.teamName)) {
               return false;
           }
           return true;
         }
		
		
		public void addHomeResult(int home_goals, int away_goals) {
			if (home_goals==away_goals) points = points + 1;  // Draw
			if (home_goals > away_goals) points = points + 3; // Home Win
			if (home_goals < away_goals) points = points + 3; // Away Win
		
		}
}